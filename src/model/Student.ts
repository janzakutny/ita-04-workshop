import Workshop from "./Workshop";

export default class Student {
    public name:string;
    public workshop:Workshop;
    public email:string;
    public employmentType: string;

    constructor() {
        this.name = 'Samuel';
        this.email = 'moj@email.com';
    }
}
