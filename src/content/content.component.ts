import {Component} from "@angular/core";
import Workshop from "../model/Workshop";
import TestService from "../service/TestService";
import Student from "../model/Student";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'content-component',
    templateUrl: './content.component.html',
    providers: [TestService]
})
export class ContentComponent {
    private formName: FormGroup;

    private student: Student = new Student();

    private workshops:Workshop[] = [{
        name: 'ITA 04 - Angular 2',
        description: 'Angular 2 workshop description',
        language: 'Javascript',
        startDate: new Date(),
        lessons: 10
    }, {
        name: 'ITA 04 - React',
        description: 'React workshop description',
        language: 'Javascript',
        startDate: new Date(),
        lessons: 10
    }, {
        name: 'ITA 04 - C#',
        description: 'C# workshop description',
        language: 'C#+',
        startDate: new Date(),
        lessons: 10
    }];

    constructor(private fb:FormBuilder) {
        this.formName = fb.group({
            name: ['', Validators.required],
            workshop: '',
            email: '',
            employmentType: ''
        });
        this.formName.patchValue(this.student);
    }

    private submit() {
        console.log(this.formName);
        this.student = this.formName.value;
        console.log(this.student);
    }

}
